var $ = jQuery;
if ($('.mv_video').length) {
	// https://developers.google.com/youtube/iframe_api_reference
	// Inject YouTube API script
	// 2. This code loads the IFrame Player API code asynchronously.
	var tag = document.createElement('script');

	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	// 3. This function creates an <iframe> (and YouTube player)
	//	after the API code downloads.
	var player;
	var is_playing = true;
	var playButton = document.getElementById("play_button");
	var pauseButton = document.getElementById("pause_button");
	var elementId = document.getElementById("mv_ytb");
	var mvId = elementId.classList[0];
	var title = elementId.getAttribute('title');
	function onYouTubeIframeAPIReady() {
		player = new YT.Player('mv_ytb', {
			height: '100%',
			width: '100%',
			videoId: mvId,
			playerVars: {
				showinfo: 0,
				controls: 0,
				autoplay: 1,
				autohide: 1,
				rel: 0,
				fs: 0,
				mute: 1,
				disablekb: 0,
				playsinline: 1,
				loop: 1,
				playlist:mvId
			},
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		});
	}
	var start = 0;
	var isPlay = false;
	var isMute = true;
	// 4. The API will call this function when the video player is ready.
	function onPlayerReady(event) {
		$('.mv_video').find('iframe').attr('title', title);
		playButton.addEventListener("click", function() {
			playVideo()
			isPlay = true;
		});
		pauseButton.addEventListener("click", function() {
			pauseVideo()
			isPlay = false;
		});
		
		if(isPlay){
			$(".mv_video").removeClass('video_stop');
			$(".mv_video").addClass('video_play');
			pauseVideo();
		}
	}
	function onPlayerStateChange(event) {
		if (event.data == YT.PlayerState.PLAYING) {
			playVideo();
			$(".mv_video").removeClass('video_stop').addClass('video_play');
		}
		if (event.data == YT.PlayerState.PAUSED) {
			player.pauseVideo();
			$(".mv_video").removeClass('video_play').addClass('video_stop');
		}
		if (event.data == YT.PlayerState.ENDED) {
			playVideo();
			playButton.addEventListener("click", function(event) {
				playVideo();
			});
		}
	}
	function windowScrollPauseVideo(){
		$(window).on('scroll', function(e) {
			var sectionVideoH = $('#header').innerHeight() + $('.mv_video').innerHeight();
			var scrollWindow = $(window).scrollTop();
			if(scrollWindow >= sectionVideoH){
				player.pauseVideo();
			}else{
				if(isPlay) player.playVideo();
			}
		})
	}
	function stopVideo() {
		player.stopVideo();
	}
	function playVideo() {
		$(".mv_video").removeClass('video_stop');
		$(".mv_video").addClass('video_play');
		player.playVideo();
	}
	function pauseVideo() {
		$(".mv_video").addClass('video_stop');
		$(".mv_video").removeClass('video_play');
		player.pauseVideo();
	}
}
// 5. The API calls this function when the player's state changes.
//	The function indicates that when playing a video (state=1),
//	the player should play for six seconds and then stop.